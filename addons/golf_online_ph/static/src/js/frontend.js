$(document).ready(function() {

    // legend pop-over
    $(function(){
        // Enables popover
        $("[data-toggle=popover]").popover();
    });

    $("#tournament-list-table").DataTable({
        "order": [[0, "desc"]]
    });

    $("#courses-list-table").DataTable({
        "order": [[0, "desc"]]
    });

    // ONGOING AND UPCOMING TOURNAMENTS TABLE ORDERED BY PLAYER NAME (INDIVIDUAL)
    $("table[id^='ongoing-system-36-individual-table-']").DataTable({
        "order": [[0, "asc"]]
    });

    $("table[id^='upcoming-system-36-individual-table-']").DataTable({
        "order": [[0, "asc"]]
    });

    // RECENT TOURNAMENTS TABLE ORDERED BY GROSS RANKING (INDIVIDUAL)
    $("table[id^='recent-system-36-individual-table-']").DataTable({
        "order": [[5, "asc"]]
    });

    // ONGOING AND UPCOMING TOURNAMENTS TABLE ORDERED BY TEAM NAME (TEAM)
    $("table[id^='ongoing-system-36-team-table-']").DataTable({
        "order": [[0, "asc"]]
    });

    $("table[id^='upcoming-system-36-team-table-']").DataTable({
        "order": [[0, "asc"]]
    });
    
    // RECENT TOURNAMENTS TABLE ORDERED BY TEAM GROSS RANKING (TEAM)
    $("table[id^='recent-system-36-team-table-']").DataTable({
        "order": [[5, "asc"]]
    });
    

    $("table[id^='flight-table-']").DataTable({
    });

    $("#team-members").DataTable({
        "order": [[0, "asc"]]
    });

    $('#main-navigation nav li').click(function(){
        $('li').removeClass("active");
        $(this).addClass("active");
    });

    var classColumn = 1;

    // SORTED BY GROSS RANKING - TABLE FOR RECENT TOURNAMENTS (INDIVIDUAL)
    var sorted_by_gross_ind_tbl = $("table[id^='recent-individual-table-']").DataTable({
        // ordered by gross ranking
        pageLength: 25,
        order: [[6, "asc"]],
        orderFixed: [[classColumn, "asc"]],
        columnDefs: [
            { "visible": false, "targets": classColumn }
        ],
        rowGroup: {
            dataSrc: classColumn
        },
    });

    // CLASSIFICATION ORDERING OF sorted_by_gross_ind_tbl (INDIVIDUAL)
    $("table[id^='recent-individual-table-'] tbody").on( 'click', 'tr.group', function () {
        var currentOrder = sorted_by_gross_ind_tbl.order()[0];
        if ( currentOrder[0] === classColumn && currentOrder[1] === 'asc' ) {
            sorted_by_gross_ind_tbl.order( [ classColumn, 'desc' ] ).draw();
        }
        else {
            sorted_by_gross_ind_tbl.order( [ classColumn, 'asc' ] ).draw();
        }
    } );

    // SORTED BY NAME - TABLE FOR ONGOING TOURNAMENTS (INDIVIDUAL)
    var ong_sorted_by_name_tbl = $("table[id^='ongoing-individual-table-']").DataTable({
        // ordered by gross ranking
        pageLength: 25,
        order: [[0, "asc"]],
        orderFixed: [[classColumn, "asc"]],
        columnDefs: [
            { "visible": false, "targets": classColumn }
        ],
        rowGroup: {
            dataSrc: classColumn
        },
    });

    // CLASSIFICATION ORDERING OF ong_sorted_by_name_tbl (INDIVIDUAL)
    $("table[id^='ongoing-individual-table-'] tbody").on( 'click', 'tr.group', function () {
        var currentOrder = ong_sorted_by_name_tbl.order()[0];
        if ( currentOrder[0] === classColumn && currentOrder[1] === 'asc' ) {
            ong_sorted_by_name_tbl.order( [ classColumn, 'desc' ] ).draw();
        }
        else {
            ong_sorted_by_name_tbl.order( [ classColumn, 'asc' ] ).draw();
        }
    } );
    
    // SORTED BY NAME - TABLE FOR UPCOMING TOURNAMENTS (INDIVIDUAL)
    var upc_sorted_by_name_tbl = $("table[id^='upcoming-individual-table-']").DataTable({
        // ordered by gross ranking
        pageLength: 25,
        order: [[0, "asc"]],
        orderFixed: [[classColumn, "asc"]],
        columnDefs: [
            { "visible": false, "targets": classColumn }
        ],
        rowGroup: {
            dataSrc: classColumn
        },
    });

    // CLASSIFICATION ORDERING OF upc_sorted_by_name_tbl (INDIVIDUAL)
    $("table[id^='upcoming-individual-table-'] tbody").on( 'click', 'tr.group', function () {
        var currentOrder = upc_sorted_by_name_tbl.order()[0];
        if ( currentOrder[0] === classColumn && currentOrder[1] === 'asc' ) {
            upc_sorted_by_name_tbl.order( [ classColumn, 'desc' ] ).draw();
        }
        else {
            upc_sorted_by_name_tbl.order( [ classColumn, 'asc' ] ).draw();
        }
    } );

    // SORTED BY GROSS RANKING - TABLE FOR RECENT TOURNAMENTS (TEAM)
    var sorted_by_gross_team_tbl = $("table[id^='recent-team-table-']").DataTable({
        // ordered by gross ranking
        pageLength: 25,
        order: [[6, "asc"]],
        orderFixed: [[classColumn, "asc"]],
        columnDefs: [
            { "visible": false, "targets": classColumn }
        ],
        rowGroup: {
            dataSrc: classColumn
        },
    });

    // CLASSIFICATION ORDERING OF sorted_by_gross_team_tbl (TEAM)
    $("table[id^='recent-team-table-'] tbody").on( 'click', 'tr.group', function () {
        var currentOrder = sorted_by_gross_team_tbl.order()[0];
        if ( currentOrder[0] === classColumn && currentOrder[1] === 'asc' ) {
            sorted_by_gross_team_tbl.order( [ classColumn, 'desc' ] ).draw();
        }
        else {
            sorted_by_gross_team_tbl.order( [ classColumn, 'asc' ] ).draw();
        }
    } );

    // SORTED BY TEAM NAME - TABLE FOR ONGOING TOURNAMENTS (TEAM)
    var upc_sorted_by_name_team = $("table[id^='upcoming-team-table-']").DataTable({
        // ordered by gross ranking
        pageLength: 25,
        order: [[0, "asc"]],
        orderFixed: [[classColumn, "asc"]],
        columnDefs: [
            { "visible": false, "targets": classColumn }
        ],
        rowGroup: {
            dataSrc: classColumn
        },
    });

    // CLASSIFICATION ORDERING OF upc_sorted_by_name_team (TEAM)
    $("table[id^='upcoming-team-table-'] tbody").on( 'click', 'tr.group', function () {
        var currentOrder = upc_sorted_by_name_team.order()[0];
        if ( currentOrder[0] === classColumn && currentOrder[1] === 'asc' ) {
            upc_sorted_by_name_team.order( [ classColumn, 'desc' ] ).draw();
        }
        else {
            upc_sorted_by_name_team.order( [ classColumn, 'asc' ] ).draw();
        }
    } );
});
