from odoo import models, fields, api
import datetime
from odoo.exceptions import ValidationError


class TournamentRounds(models.Model):
    _name = "goph.tournament_rounds"
    _rec_name = "round_number"

    round_number = fields.Char(string="Round Name")
    tournament_id = fields.Many2one("goph.tournament", ondelete="cascade", string="Tournament")
    course_id = fields.Integer(related="tournament_id.tournament_course.id")
    round_holes = fields.One2many(related="tournament_id.tournament_course.course_holes", string="Round Holes")
    round_players = fields.Many2many("goph.registered_player", string="Round Players")
    score_cards = fields.One2many("goph.player_score_card", "round_id", string="Score Cards")
    round_flights = fields.One2many("goph.tournament_flights", "round_id", string="Flights")

    @api.multi
    def write(self, vals):
        players_list = []
        if 'round_players' in vals:
            players_list = vals.get('round_players')
        # round_id = vals.get('round_division_id')
        for record in self:
            for player in players_list:
                original_player_ids = record.round_players.ids

                if player[0] == 6:
                    to_retain_ids = player[2]
                    to_remove_ids = set(to_retain_ids).symmetric_difference(
                        set(original_player_ids))
                    record.env['goph.player_score_card'].search([
                        ('tournament_id', '=', record.tournament_id.id),
                        ('registered_player_id', 'in', list(to_remove_ids)),
                        ('round_id', '=', record.id)]).unlink()

                    # updated score card
                    player_score_card = self.env['goph.player_score_card']
                    

                    for retain_id in to_retain_ids:
                        if retain_id in original_player_ids:
                            continue

                        course_holes = record.tournament_id.tournament_course.course_holes

                        num_of_holes = len(course_holes)
                        half_num_of_holes = int(num_of_holes / 2)

                        first_half_holes = self.env['goph.course_holes'].search([
                            ('course_id', '=', record.tournament_id.tournament_course.id)
                        ]).sorted('hole_number')[:half_num_of_holes]

                        second_half_holes = self.env['goph.course_holes'].search([
                            ('course_id', '=', record.tournament_id.tournament_course.id)
                        ]).sorted('hole_number')[half_num_of_holes:num_of_holes]

                        score_card_instance = player_score_card.create({
                            'registered_player_id': retain_id,
                            'round_id': record.id,
                            'tournament_id': record.tournament_id.id,
                        })

                        if record.tournament_id.tournament_scoring_format == 'system-36':
                            front_nine_system_36 = self.env['goph.front_nine_system_36']
                            back_nine_system_36 = self.env['goph.back_nine_system_36']

                            for hole in first_half_holes:
                                front_nine_system_36.create({
                                    'player_score_card_id': score_card_instance.id,
                                    'hole_id': hole.id,
                                    'stroke': 0
                                })

                            for hole in second_half_holes:
                                back_nine_system_36.create({
                                    'player_score_card_id': score_card_instance.id,
                                    'hole_id': hole.id,
                                    'stroke': 0
                                })
                        elif record.tournament_id.tournament_scoring_format == 'stroke-play':
                            front_nine_stroke_play = self.env['goph.front_nine_stroke_play']
                            back_nine_stroke_play = self.env['goph.back_nine_stroke_play']

                            for hole in first_half_holes:
                                front_nine_stroke_play.create({
                                    'player_score_card_id': score_card_instance.id,
                                    'hole_id': hole.id,
                                    'stroke': 0
                                })

                            for hole in second_half_holes:
                                back_nine_stroke_play.create({
                                    'player_score_card_id': score_card_instance.id,
                                    'hole_id': hole.id,
                                    'stroke': 0
                                })
                        else:
                            front_nine_default = self.env['goph.front_nine_default']
                            back_nine_default = self.env['goph.back_nine_default']

                            for hole in first_half_holes:
                                front_nine_default.create({
                                    'player_score_card_id': score_card_instance.id,
                                    'hole_id': hole.id,
                                    'stroke': 0
                                })

                            for hole in second_half_holes:
                                back_nine_default.create({
                                    'player_score_card_id': score_card_instance.id,
                                    'hole_id': hole.id,
                                    'stroke': 0
                                })
            super(TournamentRounds, record).write(vals)
        return True


class Classification(models.Model):
    _name = "goph.classification"
    _rec_name = "classification_name"

    classification_name = fields.Char(string="Name", required=True)
    classification_max_handicap = fields.Float(string="Max Handicap")
    classification_min_handicap = fields.Float(string="Min Handicap")

    # related fields
    eligible_division_id = fields.Many2one("goph.eligible_division_instance", ondelete="cascade")
    tournament_id = fields.Many2one(related="eligible_division_id.tournament_id", ondelete="cascade")
    
    # display fields
    individual_gross_champion = fields.Many2one("goph.registered_player", string="Gross Champion")
    individual_gross_runner_up = fields.Many2one("goph.registered_player", string="Gross Runner Up")
    individual_net_champion = fields.Many2one("goph.registered_player", string="Net Champion")
    individual_net_runner_up = fields.Many2one("goph.registered_player", string="Net Runner Up")

    team_gross_champion = fields.Many2one("goph.golf_team", string="Gross Champion")
    team_gross_runner_up = fields.Many2one("goph.golf_team", string="Gross Runner Up")
    team_net_champion = fields.Many2one("goph.golf_team", string="Net Champion")
    team_net_runner_up = fields.Many2one("goph.golf_team", string="Net Runner Up")

    
    # used for getting top players
    qualified_players = fields.One2many("goph.registered_player", "player_classification_id")
    qualified_teams = fields.One2many("goph.golf_team", "team_classification_id")

    # for filtering
    tournament_mode = fields.Selection(related="eligible_division_id.tournament_id.tournament_mode")

    def update_awardees(self, classification_obj):

        vals = {}
        
        qualified_players = classification_obj.qualified_players

        vals['individual_gross_champion'] = qualified_players.filtered(lambda x: x.player_gross_ranking == 1 and x.total_gross_score != 0).id
        vals['individual_gross_runner_up'] = qualified_players.filtered(lambda x: x.player_gross_ranking == 2 and x.total_gross_score != 0).id

        temp_net_champion = qualified_players.filtered(lambda x: x.player_net_ranking == 1 and x.total_gross_score != 0).id
        
        if temp_net_champion == vals['individual_gross_champion']:
            temp_net_champion = qualified_players.filtered(lambda x: x.player_net_ranking == 2 and x.total_gross_score != 0).id
            vals['individual_net_runner_up'] = qualified_players.filtered(lambda x: x.player_net_ranking == 3 and x.total_gross_score != 0).id
        else:
            vals['individual_net_runner_up'] = qualified_players.filtered(lambda x: x.player_net_ranking == 2 and x.total_gross_score != 0).id
        
        vals['individual_net_champion'] = temp_net_champion
        
        # update team awardees if in team mode
        if classification_obj.tournament_id.tournament_mode == 'team-mode':
            
            qualified_teams = classification_obj.qualified_teams

            vals['team_gross_champion'] = qualified_teams.filtered(lambda x: x.team_overall_gross_ranking == 1 and x.team_total_gross_score != 0).id
            vals['team_gross_runner_up'] = qualified_teams.filtered(lambda x: x.team_overall_gross_ranking == 2 and x.team_total_gross_score != 0).id

            temp_net_champion_team = qualified_teams.filtered(lambda x: x.team_overall_net_ranking == 1 and x.team_total_gross_score != 0).id
            
            if temp_net_champion_team == vals['team_gross_champion']:
                temp_net_champion_team = qualified_teams.filtered(lambda x: x.team_overall_net_ranking == 2 and x.team_total_gross_score != 0).id
                vals['team_net_runner_up'] = qualified_teams.filtered(lambda x: x.team_overall_net_ranking == 3 and x.team_total_gross_score != 0).id
            else:
                vals['team_net_runner_up'] = qualified_teams.filtered(lambda x: x.team_overall_net_ranking == 2 and x.team_total_gross_score != 0).id
            
            vals['team_net_champion'] = temp_net_champion_team

        classification_obj.write(vals)


    @api.multi
    @api.constrains('classification_max_handicap', 'classification_min_handicap')
    def _check_range(self):
        for record in self:
            classifications = record.tournament_id.tournament_classifications

            for classification in classifications:
                if classification.classification_min_handicap >= classification.classification_max_handicap:
                    raise ValidationError(
                        "Classification Max Handicap should not be less than or equal the minimum")


class EligibleDivision(models.Model):
    _name = "goph.eligible_division"
    _rec_name = "eligible_division_name"

    eligible_division_name = fields.Char(string="Name", required=True)
    is_default = fields.Boolean()


class TournamentEligibleDivisions(models.Model):
    _name = "goph.eligible_division_instance"
    _rec_name = "eligible_division_id"

    eligible_division_id = fields.Many2one("goph.eligible_division", ondelete="cascade")
    tournament_id = fields.Many2one("goph.tournament", ondelete="cascade")
    point_system_instances = fields.One2many("goph.point_system_instance", "eligible_division_instance_id")
    classifications = fields.One2many("goph.classification", "eligible_division_id")

    qualified_players = fields.One2many("goph.registered_player", "main_division_id")
    qualified_teams = fields.One2many("goph.golf_team", "main_division_id")

    # awardees (individual)
    individual_gross_champion = fields.Many2one("goph.registered_player", string="Gross Champion")
    individual_gross_runner_up = fields.Many2one("goph.registered_player", string="Gross Runner Up")
    individual_net_champion = fields.Many2one("goph.registered_player", string="Net Champion")
    individual_net_runner_up = fields.Many2one("goph.registered_player", string="Net Runner Up")

    # awardees (team)
    team_gross_champion = fields.Many2one("goph.golf_team", string="Gross Champion")
    team_gross_runner_up = fields.Many2one("goph.golf_team", string="Gross Runner Up")
    team_net_champion = fields.Many2one("goph.golf_team", string="Net Champion")
    team_net_runner_up = fields.Many2one("goph.golf_team", string="Net Runner Up")

    # for filtering
    tournament_scoring_format = fields.Selection(related="tournament_id.tournament_scoring_format")
    tournament_mode = fields.Selection(related="tournament_id.tournament_mode")

    @api.multi
    @api.constrains('classifications')
    def _check_ranges(self):
        for record in self:
            classifications = record.classifications

            for classification in classifications:
                current_min_handicap = classification.classification_min_handicap
                current_max_handicap = classification.classification_max_handicap
                other_classifications = record.classifications.filtered(lambda r: r.id != classification.id)
                
                for other_classification in other_classifications:
                    if (current_min_handicap >= other_classification.classification_min_handicap) and (current_min_handicap <= other_classification.classification_max_handicap):
                        raise ValidationError("Conflicting Handicap Range in one of your classifications.")
                    
                    if (current_max_handicap >= other_classification.classification_min_handicap) and (current_max_handicap <= other_classification.classification_max_handicap):
                        raise ValidationError("Conflicting Handicap Range in one of your classifications.")

    def update_awardees(self, main_division_obj):

        vals = {}

        qualified_players = main_division_obj.qualified_players

        vals['individual_gross_champion'] = qualified_players.filtered(lambda x: x.overall_gross_ranking == 1 and x.total_gross_score != 0).id
        vals['individual_gross_runner_up'] = qualified_players.filtered(lambda x: x.overall_gross_ranking == 2 and x.total_gross_score != 0).id

        temp_net_champion = qualified_players.filtered(lambda x: x.overall_net_ranking == 1 and x.total_gross_score != 0).id
        
        if temp_net_champion == vals['individual_gross_champion']:
            temp_net_champion = qualified_players.filtered(lambda x: x.overall_net_ranking == 2 and x.total_gross_score != 0).id
            vals['individual_net_runner_up'] = qualified_players.filtered(lambda x: x.overall_net_ranking == 3 and x.total_gross_score != 0).id
        else:
            vals['individual_net_runner_up'] = qualified_players.filtered(lambda x: x.overall_net_ranking == 2 and x.total_gross_score != 0).id
        
        vals['individual_net_champion'] = temp_net_champion

        # update team awardees if in team mode
        if main_division_obj.tournament_id.tournament_mode == 'team-mode':
            
            qualified_teams = main_division_obj.qualified_teams

            vals['team_gross_champion'] = qualified_teams.filtered(lambda x: x.team_overall_gross_ranking == 1 and x.team_total_gross_score != 0).id
            vals['team_gross_runner_up'] = qualified_teams.filtered(lambda x: x.team_overall_gross_ranking == 2 and x.team_total_gross_score != 0).id

            temp_net_champion_team = qualified_teams.filtered(lambda x: x.team_overall_net_ranking == 1 and x.team_total_gross_score != 0).id
            
            if temp_net_champion_team == vals['team_gross_champion']:
                temp_net_champion_team = qualified_teams.filtered(lambda x: x.team_overall_net_ranking == 2 and x.team_total_gross_score != 0).id
                vals['team_net_runner_up'] = qualified_teams.filtered(lambda x: x.team_overall_net_ranking == 3 and x.team_total_gross_score != 0).id
            else:
                vals['team_net_runner_up'] = qualified_teams.filtered(lambda x: x.team_overall_net_ranking == 2 and x.team_total_gross_score != 0).id
            
            vals['team_net_champion'] = temp_net_champion_team
        
        main_division_obj.write(vals)


class PointSystemInstance(models.Model):
    _name = "goph.point_system_instance"
    _rec_name = "point_system_name"

    point_system_name = fields.Char(string="Name", required=True)
    points = fields.Integer(string="Points", default=0, required=True)
    eligible_division_instance_id = fields.Many2one("goph.eligible_division_instance", ondelete="cascade")
    value = fields.Integer(string="Value", default=0, required=True)

class Sponsor(models.Model):
    _name = "goph.sponsor"
    _rec_name = "sponsor_name"

    sponsor_image = fields.Binary(string="Sponsor Image")
    sponsor_name = fields.Char(string="Name", required=True)
    sponsor_address = fields.Char(string="Address")
    sponsor_contact = fields.Char(string="Contact Number")
    sponsor_website = fields.Char()