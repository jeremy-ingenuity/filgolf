# from datetime import date
from odoo import models, fields, api
from datetime import datetime as dt
from odoo.exceptions import ValidationError


class Tournament(models.Model):
    _name = "goph.tournament"
    _rec_name = "tournament_name"

    tournament_name = fields.Char(string="Tournament Name", required=True)
    tournament_course = fields.Many2one("goph.course", string="Course", required=True, ondelete="cascade")
    tournament_start_sched = fields.Datetime(string="Start Date", required=True, default=fields.Datetime.now())
    tournament_end_sched = fields.Datetime(string="End Date", required=True, default=fields.Datetime.now())
    tournament_image = fields.Binary(string="Tournament Banner")
    
    tournament_mode = fields.Selection([
            ("team-mode", "Team Mode"),
            ("individual-mode", "Individual Mode")
        ], string="Tournament Mode", required=True, default="individual-mode")
    
    tournament_scoring_format = fields.Selection([
            ("modified-stableford", "Modified Stableford"),
            ("molave", "Molave Scoring Format"),
            ("system-36", "System 36"),
            ("stroke-play", "Stroke Play"),
        ], string="Scoring Format", default="modified-stableford", required=True)

    # for scheduling
    is_automatic_scheduling = fields.Boolean(string="Automatic Scheduling?", default=True)
    tee_of_time_start = fields.Float(string="Start Time (24-hr Format)")
    tee_of_time_end = fields.Float(string="End Time (24-hr Format)")
    scheduling_interval = fields.Integer(string="Interval (Minutes)", default=10)
    players_per_time = fields.Integer(string="No. of Players Per Time", default=5)
    tournament_tee_of_format = fields.Selection([
            ("shotgun", "Shotgun"),
            # ("come-as-you-play", "Come As You Play"),
            # ("double-barrel", "Double Barrel")
        ], string="Tee Of Format", default="shotgun", required=True)

    # related fields
    tournament_eligible_divisions = fields.Many2many("goph.eligible_division")
    tournament_classifications = fields.One2many("goph.classification", "tournament_id", string="Divisions")
    tournament_rounds = fields.One2many("goph.tournament_rounds", "tournament_id",string="Tournament Rounds")
    tournament_golf_players = fields.Many2many("goph.golf_player", string="Players")

    # team mode setup
    tournament_golf_teams = fields.One2many("goph.golf_team", "tournament_id", string="Teams")
    tournament_handicap_calculator = fields.Selection([
            ("ave", "Team Handicap Average"),
            ("ave-mult", "Team Handicap Average (with multiplier)")
        ], string="Multiplier", default="ave", required=True)
    custom_handicap_multiplier = fields.Float(string="Multiplier (in %)", default=1)
    max_team_members = fields.Integer(default=10, string="Maximum Team Members")
    min_team_members = fields.Integer(default=5, string="Minimum Team Members")
    number_of_scorers_counted = fields.Integer(default=3, string="No. of Scorers Counted")

    # computed fields
    formatted_date = fields.Char(compute="_format_date", string="Date")
    tournament_status = fields.Char(compute="_get_tournament_status", readonly=True, default="upcoming")

    active_registered_players = fields.One2many("goph.registered_player", "tournament_id")
    active_main_divisions = fields.One2many("goph.eligible_division_instance", "tournament_id")

    # OTHER DETAILS
    tournament_sponsors = fields.Many2many("goph.sponsor", string="Sponsors")
    tournament_rules = fields.Html(string="Rules")
    other_details = fields.Html(string="Tournament Details")
    tournament_awards = fields.One2many("goph.award", "tournament_id", string="Awards")
    tournament_giveaways = fields.One2many("goph.giveaway", "tournament_id", string="Giveaways")
    free_admission = fields.Boolean(string="Free Admission?", default=False) 
    tournament_ticket_price = fields.Float(string="Ticket Price")


    @api.one
    @api.depends('tournament_start_sched', 'tournament_end_sched')
    def _get_tournament_status(self):
        start_sched = dt.strptime(self.tournament_start_sched, '%Y-%m-%d %H:%M:%S').date()
        end_sched = dt.strptime(self.tournament_end_sched, '%Y-%m-%d %H:%M:%S').date()
        today_date = dt.now().date()

        if end_sched < today_date:
            self.tournament_status = 'Recent'
        elif (start_sched <= today_date and end_sched >= today_date):
            self.tournament_status = 'Ongoing'
        else:
            self.tournament_status = 'Upcoming'

    def get_default_divisions(self):
        return self.env["goph.eligible_division"].search([])

    @api.constrains('tournament_start_sched', 'tournament_end_sched')
    def _check_dates(self):
        for record in self:
            if record.tournament_start_sched > record.tournament_end_sched:
                raise ValidationError(
                    "End Schedule must not come before Start Schedule.")
    
    @api.constrains('number_of_scorers_counted', 'max_team_members', 'min_team_members' )
    def _check_number_of_scorers_counted(self):
        for record in self:
            if record.number_of_scorers_counted not in range(1, (record.max_team_members+1)):
                raise ValidationError(
                    "Number of Scorers Counted should not exceed max team members.")
            
            if record.max_team_members <= record.min_team_members:
                raise ValidationError(
                    "Minimum number of members should not exceed max number of members.")

    @api.constrains('tournament_golf_teams')
    def _check_players(self):
        for record in self:
            # import pdb; pdb.set_trace()
            for team in record.tournament_golf_teams:
                current_member_ids = team.team_members.ids
                member_ids_from_other_teams = []
                registered_player_obj = self.env[
                    'goph.registered_player'].search([
                        ('tournament_id', '=', record.id),
                        ('team_id', '!=', team.id)
                    ])
                for registered_player in registered_player_obj:
                    member_ids_from_other_teams.append(
                        registered_player.player_id.id)

                if [i for i in current_member_ids if i in member_ids_from_other_teams]:
                    raise ValidationError(
                        "A duplicate player exists on one of your registered teams")

    # @api.one is a decorator that loops automatically on Records of Recordset.
    # Self is redefined as the current record
    @api.one
    @api.depends('tournament_end_sched', 'tournament_start_sched')
    def _format_date(self):
        start_date = dt.strptime(self.tournament_start_sched, '%Y-%m-%d %H:%M:%S').date()
        end_date = dt.strptime(self.tournament_end_sched, '%Y-%m-%d %H:%M:%S').date()

        converted_start_date = start_date.strftime('%b. %d')
        converted_end_date = end_date.strftime('%b. %d, %Y')

        if start_date == end_date:
            self.formatted_date = end_date.strftime('%b. %d, %Y')
        else:
            # compare months
            start_month = start_date.strftime('%b. ')
            end_month =  end_date.strftime('%b. ')
            start_day = start_date.strftime('%d')
            end_day = end_date.strftime('%d')
            year = end_date.strftime('%Y')

            if start_month == end_month:
                self.formatted_date = start_month + start_day + " - " + end_day + ", " + year
            else:
                self.formatted_date = start_month + start_day + " - " + end_month + end_day + ", " + year

    @api.model
    def create(self, vals):
        registered_player_obj = self.env['goph.registered_player']
        tournament_eligible_division = self.env['goph.eligible_division_instance']
        point_system_obj = self.env['goph.point_system_instance']
        team_rounds_obj = self.env['goph.team_rounds']

        new_obj = super(Tournament, self).create(vals)

        for eligible_division in new_obj.tournament_eligible_divisions:

            created_eligible_division = tournament_eligible_division.create({
                'eligible_division_id': eligible_division.id,
                'tournament_id': new_obj.id
            })
            if (new_obj.tournament_scoring_format == 'molave' or
                new_obj.tournament_scoring_format == 'modified-stableford'):

                point_system_obj = self.env['goph.point_system_instance']

                point_system_obj.create({
                    'point_system_name': '4 Strokes Under Par (Hole-In-One)',
                    'points': 7,
                    'value': -4,
                    'eligible_division_instance_id': created_eligible_division.id
                })
                point_system_obj.create({
                    'point_system_name': '3 Strokes Under Par (Double Eagle)',
                    'points': 6,
                    'value': -3,
                    'eligible_division_instance_id': created_eligible_division.id
                })
                point_system_obj.create({
                    'point_system_name': '2 Strokes Under Par (Eagle)',
                    'points': 5,
                    'value': -2,
                    'eligible_division_instance_id': created_eligible_division.id
                })
                point_system_obj.create({
                    'point_system_name': '1 Stroke Under Par (Birdie)',
                    'points': 4,
                    'value': -1,
                    'eligible_division_instance_id': created_eligible_division.id
                })
                point_system_obj.create({
                    'point_system_name': 'Par',
                    'points': 3,
                    'value': 0,
                    'eligible_division_instance_id': created_eligible_division.id
                })
                point_system_obj.create({
                    'point_system_name': '1 Stroke Over Par (Bogey)',
                    'points': 2,
                    'value': 1,
                    'eligible_division_instance_id': created_eligible_division.id
                })
                point_system_obj.create({
                    'point_system_name': '2 Strokes Over Par (Double Bogey)',
                    'points': 1,
                    'value': 2,
                    'eligible_division_instance_id': created_eligible_division.id
                })


        if new_obj.tournament_mode == "individual-mode":  # individual mode
            for player in new_obj.tournament_golf_players:
                if new_obj.tournament_scoring_format == 'system-36':
                    handicap = 0
                else:
                    handicap = player.player_handicap
                created_player = registered_player_obj.create({
                    'player_id': player.id,
                    'tournament_id': new_obj.id,
                    'player_handicap': handicap,
                })
                created_player.main_division_id = new_obj.set_main_division(created_player)
            return new_obj
        else:  # team mode
            # creating team instances 
            for team in new_obj.tournament_golf_teams:
                team.main_division_id = new_obj.set_default_team_division(new_obj)
                
                # creating player instances for each team member
                for team_member in team.team_members:
                    if new_obj.tournament_scoring_format == 'system-36':
                        handicap = 0
                    else:
                        handicap = team_member.player_handicap
                    registered_player_obj.create({
                        'team_id': team.id,
                        'player_id': team_member.id,
                        'tournament_id': new_obj.id,
                        'player_handicap': handicap,
                        'main_division_id': team.main_division_id.id,
                    })
                # creating team rounds for each team
                for tour_round in team.tournament_id.tournament_rounds:
                    team_rounds_obj.create({
                        'team_id': team.id,
                        'round_id': tour_round.id,
                    })

            return new_obj


    def set_default_team_division(self, tournament):
        
        main_division_instances = tournament.active_main_divisions

        regular_division_index = main_division_instances.filtered(
                lambda r: r.eligible_division_id == self.env.ref('golf_online_ph.regular-division')).id

        senior_division_index = main_division_instances.filtered(
            lambda r: r.eligible_division_id == self.env.ref('golf_online_ph.senior-division')).id

        ladies_division_index = main_division_instances.filtered(
            lambda r: r.eligible_division_id == self.env.ref('golf_online_ph.ladies-division')).id

        mens_division_index = main_division_instances.filtered(
            lambda r: r.eligible_division_id == self.env.ref('golf_online_ph.mens-division')).id        

        if regular_division_index:
            return regular_division_index
        elif senior_division_index:
            return senior_division_index
        elif ladies_division_index:
            return ladies_division_index
        else:
            return mens_division_index


    def set_main_division(self, registered_player):

        main_division_instances = registered_player.tournament_id.active_main_divisions

        regular_division_index = main_division_instances.filtered(
                lambda r: r.eligible_division_id == self.env.ref('golf_online_ph.regular-division')).id

        senior_division_index = main_division_instances.filtered(
            lambda r: r.eligible_division_id == self.env.ref('golf_online_ph.senior-division')).id

        ladies_division_index = main_division_instances.filtered(
            lambda r: r.eligible_division_id == self.env.ref('golf_online_ph.ladies-division')).id

        mens_division_index = main_division_instances.filtered(
            lambda r: r.eligible_division_id == self.env.ref('golf_online_ph.mens-division')).id

        if registered_player.player_id.player_age >= 55:
            if senior_division_index:
               return senior_division_index
            else:
               return regular_division_index

        elif registered_player.player_id.player_gender == "female":
            if ladies_division_index:
               return ladies_division_index
            else:
               return regular_division_index
        elif registered_player.player_id.player_gender == "male":
            if mens_division_index:
               return mens_division_index
            else:
               return regular_division_index
        else:
           return regular_division_index

    @api.multi
    def write(self, vals):
        players_list = []
        eligible_division_list = []
        tournament_rounds_list = []
        # import pdb; pdb.set_trace()
        if 'tournament_golf_players' in vals:
            players_list = vals.get('tournament_golf_players')
        if 'tournament_eligible_divisions' in vals:
            eligible_division_list = vals.get('tournament_eligible_divisions')
        if 'tournament_rounds' in vals:
            tournament_rounds_list = vals.get('tournament_rounds')
        for rec in self:
            for player in players_list:
                original_player_ids = rec.tournament_golf_players.ids

                if player[0] == 6:
                    to_retain_ids = player[2]
                    to_remove_ids = set(to_retain_ids).symmetric_difference(
                        set(original_player_ids))
                    rec.env['goph.registered_player'].search([
                        ('tournament_id', '=', rec.id),
                        ('player_id', 'in', list(to_remove_ids))]).unlink()

                    individual_player_obj = self.env['goph.registered_player']
                    for retain_id in to_retain_ids:
                        if retain_id in original_player_ids:
                            continue
                        golf_player = self.env['goph.golf_player'].browse(retain_id)
                        if rec.tournament_scoring_format == 'system-36':
                            handicap = 0
                        else:
                            handicap = golf_player.player_handicap
                        created_player = individual_player_obj.create({
                            'player_id': retain_id,
                            'tournament_id': rec.id,
                            'player_handicap': handicap
                        })
                        created_player.main_division_id = rec.set_main_division(created_player)

            for division in eligible_division_list:
                original_division_ids = rec.tournament_eligible_divisions.ids

                if division[0] == 6:
                    to_retain_ids = division[2]
                    to_remove_ids = set(to_retain_ids).symmetric_difference(
                        set(original_division_ids))
                    rec.env['goph.eligible_division_instance'].search([
                        ('tournament_id', '=', rec.id),
                        ('eligible_division_id', 'in', list(to_remove_ids))]).unlink()

                    division_instance_obj = self.env['goph.eligible_division_instance']
                    point_system_obj = self.env['goph.point_system_instance']

                    for retain_id in to_retain_ids:
                        if retain_id in original_division_ids:
                            continue

                        created_division_instance = division_instance_obj.create({
                            'eligible_division_id': retain_id,
                            'tournament_id': rec.id,
                        })
                        # create default point system for molave/modified stableford
                        if (rec.tournament_scoring_format == 'molave' or
                                rec.tournament_scoring_format == 'modified-stableford'):
                            point_system_obj.create({
                                'point_system_name': '4 Strokes Under Par (Hole-In-One)',
                                'points': 7,
                                'value': -4,
                                'eligible_division_instance_id': created_division_instance.id
                            })
                            point_system_obj.create({
                                'point_system_name': '3 Strokes Under Par (Double Eagle)',
                                'points': 6,
                                'value': -3,
                                'eligible_division_instance_id': created_division_instance.id
                            })
                            point_system_obj.create({
                                'point_system_name': '2 Strokes Under Par (Eagle)',
                                'points': 5,
                                'value': -2,
                                'eligible_division_instance_id': created_division_instance.id
                            })
                            point_system_obj.create({
                                'point_system_name': '1 Stroke Under Par (Birdie)',
                                'points': 4,
                                'value': -1,
                                'eligible_division_instance_id': created_division_instance.id
                            })
                            point_system_obj.create({
                                'point_system_name': 'Par',
                                'points': 3,
                                'value': 0,
                                'eligible_division_instance_id': created_division_instance.id
                            })
                            point_system_obj.create({
                                'point_system_name': '1 Stroke Over Par (Bogey)',
                                'points': 2,
                                'value': 1,
                                'eligible_division_instance_id': created_division_instance.id
                            })
                            point_system_obj.create({
                                'point_system_name': '2 Strokes Over Par (Double Bogey)',
                                'points': 1,
                                'value': 2,
                                'eligible_division_instance_id': created_division_instance.id
                            })
            
            
            
            # create instance of team round
            original_teams = rec.tournament_golf_teams.ids
            original_rounds = rec.tournament_rounds.ids
            
        
            super(Tournament, rec).write(vals)

            for team in rec.tournament_golf_teams:
                # creating team rounds for each team
                for tour_round in rec.tournament_rounds:
                    # searching if there is an existing team round
                    existing_team_round = self.env['goph.team_rounds'].search([
                        ('team_id','=', team.id),
                        ('round_id', '=', tour_round.id)
                    ]).ids

                    if existing_team_round:
                        continue
                    else:    
                        team_rounds_obj = self.env['goph.team_rounds']
                        team_rounds_obj.create({
                            'team_id': team.id,
                            'round_id': tour_round.id,
                        })
                
                if team.id in original_teams:
                    continue
                else:
                    team.main_division_id = rec.set_default_team_division(rec)
                    team.update_classification(team)
                    # creating player instances for each team member
                    for team_member in team.team_members:
                        
                        if rec.tournament_scoring_format == 'system-36':
                            handicap = 0
                        else:
                            handicap = team_member.player_handicap
                        
                        
                        registered_player_obj = self.env['goph.registered_player'].create({
                            'team_id': team.id,
                            'player_id': team_member.id,
                            'tournament_id': rec.id,
                            'player_handicap': handicap,
                            'main_division_id': team.main_division_id.id,
                        })
                        registered_player_obj.set_classification_id(registered_player_obj)

                

        return True

    _defaults = {
        'tournament_eligible_divisions': get_default_divisions
    }


class Sponsor(models.Model):
    _name = "goph.sponsor"
    _rec_name = "sponsor_name"

    sponsor_image = fields.Binary(string="Sponsor Image")
    sponsor_name = fields.Char(string="Name", required=True)
    sponsor_website = fields.Char(string="Website", default="http://")
    sponsor_contact = fields.Char(string="Contact Number") 


class Award(models.Model):
    _name = "goph.award"
    _rec_name = "award_name"

    award_for = fields.Char(string="Award For", required=True)
    award_name = fields.Char(string="Item Name", required=True)
    award_image = fields.Binary(string="Image (Optional)")
    tournament_id = fields.Many2one("goph.tournament")

class Giveaway(models.Model):
    _name = "goph.giveaway"
    _rec_name = "giveaway_name"

    giveaway_for = fields.Char(string="Giveaway For", required=True)
    giveaway_name = fields.Char(string="Item Name", required=True)
    giveaway_image = fields.Binary(string="Image (Optional)")
    tournament_id = fields.Many2one("goph.tournament")