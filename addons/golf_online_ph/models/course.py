from odoo import models, fields, api
from odoo.exceptions import ValidationError


class GolfCourse(models.Model):
    _name = "goph.course"
    _rec_name = "course_name"

    course_name = fields.Char(string="Golf Course Name", required=True)
    course_details = fields.Text(string="Course Details")
    course_address = fields.Text(string="Course Address", required=True)
    course_contact_number = fields.Char(string="Contact Number")
    course_contact_person = fields.Char(string="Contact Person")

    course_holes = fields.One2many("goph.course_holes", "course_id", string="Holes")
    course_num_of_holes = fields.Integer(string="Total # of Holes", compute="_num_of_holes", default=0)
    course_total_pars = fields.Integer(string="Total Par", compute="_total_par", default=0)
    course_tournaments = fields.One2many("goph.tournament", "tournament_course", string="List of Tournaments")

    # sets the number of holes based on the records of holes
    # associated to a golf course
    @api.depends("course_holes")
    def _num_of_holes(self):
        for record in self:
            record.course_num_of_holes = len(record.course_holes)

    # sets the total value of par based on the sum of pars for each hole
    # associated to a golf course
    @api.depends("course_holes")
    def _total_par(self):
        for record in self:
            total_par = 0
            for hole in record.course_holes:
                total_par += hole.hole_par
            record.course_total_pars = total_par

    @api.constrains("course_holes")
    def _check_hole_handicap(self):
        for record in self:
            for hole in record.course_holes:
                other_holes_number = []
                other_holes_handicap = []

                other_holes_obj = self.env['goph.course_holes'].search([
                    ('course_id', '=', record.id),
                    ('id', '!=', hole.id)
                ])

                for other_hole in other_holes_obj:
                    other_holes_number.append(str(other_hole.hole_number))
                    other_holes_handicap.append(str(other_hole.hole_handicap))


                if str(hole.hole_handicap) in other_holes_handicap:
                    raise ValidationError("Some holes have the same handicap, please check again")

                if str(hole.hole_number) in other_holes_number:
                    raise ValidationError("Some holes have the same hole number, please check again")

class GolfCourseHoles(models.Model):
    _name = "goph.course_holes"
    _rec_name = "hole_number"

    hole_number = fields.Integer(string="Hole Number", required=True)
    hole_par = fields.Integer(string="Par Value", required=True)
    hole_handicap = fields.Integer(string="Handicap")
    course_id = fields.Many2one("goph.course", string="Golf Course", ondelete="cascade")

    round_id = fields.Many2one("goph.tournament_rounds")
